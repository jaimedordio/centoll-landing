import Head from "next/head";
import styles from "../styles/Home.module.css";
import TweetEmbed from "react-tweet-embed";

const Home = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Soon...</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <div className={styles.tweet}>
          <TweetEmbed id="1189517338925764608" options={{ theme: "dark" }} />
        </div>
        <h1 className={styles.title}>
          Soon, <a href="https://twitter.com/paulg">Paul</a>
        </h1>
        <p className={styles.description}>One cart.</p>
      </main>

      <footer className={styles.footer}>
        <p>
          Powered by{" "}
          <img
            src="https://f000.backblazeb2.com/file/centoll/Centoll_Full_Logo_White_Cropped.svg"
            alt="Centoll Logo"
            className={styles.logo}
          />
        </p>
      </footer>
    </div>
  );
};

export default Home;
